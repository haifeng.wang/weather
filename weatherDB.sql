CREATE TABLE algeria (
  city_id integer NOT NULL,
  city_name character varying(50) DEFAULT NULL,
  state_code integer DEFAULT NULL,
  state_name character varying(50) DEFAULT NULL,
  country_code character varying(30) DEFAULT NULL,
  country_name character varying(50) DEFAULT NULL,
  large_city character varying(10) DEFAULT NULL,
  small_city character varying(10) DEFAULT NULL,
  PRIMARY KEY (city_id)
);

-- \copy algeria FROM  'algeria_cities.csv' DELIMITER ',' CSV;

CREATE TABLE weather (
  id SERIAL,
  wind_spd integer DEFAULT NULL,
  wind_cdir character varying(30) DEFAULT NULL,
  wind_cdir_full character varying(30) DEFAULT NULL,
  ts integer DEFAULT NULL,
  weather_code integer DEFAULT NULL,
  weather_desc character varying(100) DEFAULT NULL,
  max_temp float(4) DEFAULT NULL,
  datetime timestamptz DEFAULT NULL,
  min_temp float(4) DEFAULT NULL,
  clouds integer DEFAULT NULL,
  vis integer DEFAULT NULL,
  city_name character varying(50) DEFAULT NULL,
  PRIMARY KEY (id)
);
