package com.bfd.dao;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;

import com.alibaba.druid.pool.DruidDataSource;

/**
 * 数据源配置
 * 
 */
@Component
@ConfigurationProperties(prefix="db")
@MapperScan(basePackages = "com.bfd", sqlSessionFactoryRef = "SqlSessionFactory")
public class DataSourceConfig {

  @Value("${db.jdbc_url}")
  private String dbUrl;
  @Value("${db.jdbc_user}")
  private String dbUser;
  @Value("${db.jdbc_password}")
  private String dbPassword;
  @Value("${db.driver}")
  private String driverName;
  
 /* 
//配置初始化大小、最小、最大
  @Value("${jdbc.initialSize}")
  private Integer initialSize;
  @Value("${jdbc.minIdle}")
  private Integer minIdle;
  @Value("${jdbc.maxActive}")
  private Integer maxActive;
  // 配置获取连接等待超时的时间
  @Value("${jdbc.maxWait}")
  private long maxWait;
  // 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
  @Value("${jdbc.timeBetweenEvictionRunsMillis}")
  private long timeBetweenEvictionRunsMillis;
  // 配置一个连接在池中最小生存的时间，单位是毫秒
  @Value("${jdbc.minEvictableIdleTimeMillis}")
  private long minEvictableIdleTimeMillis;
  // 配置测试属性
  @Value("${jdbc.testWhileIdle}")
  private boolean testWhileIdle;
  @Value("${jdbc.testOnBorrow}")
  private boolean testOnBorrow;
  @Value("${jdbc.testOnReturn}")
  private boolean testOnReturn;

*/
  @Primary
  @Bean(name = "DataSource")
  public DataSource dwDataSource() {
    DruidDataSource dataSource = new DruidDataSource();
    dataSource.setUrl(dbUrl);
    dataSource.setPassword(dbPassword);
    dataSource.setUsername(dbUser);
    dataSource.setDriverClassName(driverName);
    return dataSource;
  }

  @Primary
  @Bean(name = "TransactionManager")
  public DataSourceTransactionManager bsTransactionManager(@Qualifier("DataSource") DataSource dwDataSource) {
    return new DataSourceTransactionManager(dwDataSource);
  }

  @Primary
  @Bean(name = "SqlSessionFactory")
  public SqlSessionFactory bsSqlSessionFactory(@Qualifier("DataSource") DataSource dataSource)throws Exception {
    final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
    sessionFactory.setDataSource(dataSource);
    return sessionFactory.getObject();
  }
}
