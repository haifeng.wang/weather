package com.bfd.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.stereotype.Component;

import com.bfd.pojo.WeatherPO;


@MapperScan
public interface WeatherMapper {
	
	@Insert("insert into weather(wind_Spd,wind_Cdir,wind_Cdir_Full,ts,weather_Desc,max_Temp,dateTime,min_Temp,clouds,vis,city_Name,weather_code)"
			+ "values(#{windSpd},#{windCdir},#{windCdirFull},#{ts},#{weatherDesc},#{maxTemp},#{dateTime},#{minTemp},#{clouds},"
			+ "#{vis},#{cityName},#{weatherCode})")
	public void saveWeather(WeatherPO weather);
	
	@Delete("delete  from weather where current_date-Date(datetime)>7")
	public void deleteHistoricalInfo();
	
	@Update("update weather  set wind_spd=#{windSpd}, wind_cdir=#{windCdir}, wind_cdir_full=#{windCdirFull}, ts=#{ts}, weather_code=#{weatherCode}, " + 
			"weather_desc=#{weatherDesc}, max_temp=#{maxTemp}, min_temp=#{minTemp},  clouds=#{clouds}, vis=#{vis} "
			+ "where city_name=#{cityName} and datetime=#{dateTime}")
	public void updateWeather(WeatherPO weather);
	
	@Select("select count(1) from weather  where city_name=#{cityName} and datetime=#{dateTime}")
	public int selectWeather(WeatherPO weather);

	@Select("select count(1) from weather")
	public int count();
}
