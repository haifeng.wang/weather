package com.bfd.service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bfd.dao.WeatherMapper;
import com.bfd.pojo.WeatherPO;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class WeatherService {

	@Autowired
	private WeatherMapper weatherMapper;

	public void saveWeatherInfo(String weatherInfo, Integer cityId) {
		if (null != weatherInfo) {
			List<WeatherPO> list = convertJsonToWeather(weatherInfo, cityId);
			for (WeatherPO weather : list) {
				if(0 == weatherMapper.selectWeather(weather)){
					weatherMapper.saveWeather(weather);
				}
				else {
					weatherMapper.updateWeather(weather);
				}
			}
		}

	}
	
	public int count(){
		return weatherMapper.count();
	}

	/**
	 * 删除过往七天的天气数据
	 */
	public void deleteHistoricalInfo() {
		weatherMapper.deleteHistoricalInfo();
	}

	/**
	 * json-->java bean
	 * 
	 * @param weatherInfo
	 * @return
	 */
	private static List<WeatherPO> convertJsonToWeather(String weatherInfo, Integer cityId) {
		if (null == weatherInfo || weatherInfo.isEmpty()) {
			return null;
		}
		List<WeatherPO> weatherList = new ArrayList<WeatherPO>();
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			HashMap weatherMap = objectMapper.readValue(weatherInfo, HashMap.class);
			String cityName = (String) weatherMap.get("city_name");
			List<HashMap> list = (List<HashMap>) weatherMap.get("data");
			WeatherPO weather = null;
			Date today = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(today);
			int  day1 = calendar.get(Calendar.DAY_OF_YEAR);//现在时间
			SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd");
			for (HashMap map : list) {
				weather = new WeatherPO();
				String datetime = String.valueOf(map.get("datetime"));
				try {
					Date date = format.parse(datetime);
					calendar.setTime(date);
					int  day2 = calendar.get(Calendar.DAY_OF_YEAR);
					if(day2-day1 >= 7){
						continue;
					}
					weather.setDateTime(date);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				weather.setWindCdir((String) map.get("wind_cdir"));
				weather.setWindCdirFull((String) map.get("wind_cdir_full"));
				weather.setWindSpd(Float.valueOf(String.valueOf(map.get("wind_spd"))));
				weather.setTs((Integer) map.get("ts"));
				// weatherDesc在weather中，需要解析
				Map<String, String> wea = (Map<String, String>) map.get("weather");
				String description = wea.get("description");
				Integer weatherCode = Integer.valueOf(String.valueOf(wea.get("code")));
				weather.setWeatherCode(weatherCode);
				weather.setWeatherDesc(description);
				weather.setClouds((Integer) map.get("clouds"));
				weather.setMaxTemp((Float.valueOf(String.valueOf(map.get("max_temp")))));
				weather.setMinTemp(Float.valueOf((String.valueOf(map.get("min_temp")))));
				weather.setVis(Float.valueOf(String.valueOf(map.get("vis"))));
				weather.setCityId(cityId);
				weather.setCityName(cityName);
				weatherList.add(weather);
			}
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return weatherList;
	}
}
