package com.bfd.pojo;

import java.util.Date;

public class WeatherPO {
	
	//风速（默认米/秒）
	private Float windSpd;
	//风向缩写
	private String windCdir;
	//口头风向
	private String windCdirFull;
	//最后一次观察时间(unix时间戳)
	private Integer  ts;
	//天气描述文本
	private String weatherDesc;
	//最大温度
	private Float maxTemp;
	//当前周期，表示是哪一天的数据
	private Date dateTime;
	//最小温度（Celcius）
	private Float minTemp;
	//云覆盖率（％）
	private Integer clouds;
	//可见度（默认KM）
	private Float vis;
	private Integer cityId;
	private String cityName;
	private Integer weatherCode;
	public String getWindCdir() {
		return windCdir;
	}
	public void setWindCdir(String windCdir) {
		this.windCdir = windCdir;
	}
	public Float getWindSpd() {
		return windSpd;
	}
	public void setWindSpd(Float windSpd) {
		this.windSpd = windSpd;
	}
	public Float getMaxTemp() {
		return maxTemp;
	}
	public void setMaxTemp(Float maxTemp) {
		this.maxTemp = maxTemp;
	}
	public Float getMinTemp() {
		return minTemp;
	}
	public void setMinTemp(Float minTemp) {
		this.minTemp = minTemp;
	}
	public Integer getClouds() {
		return clouds;
	}
	public void setClouds(Integer clouds) {
		this.clouds = clouds;
	}
	public Float getVis() {
		return vis;
	}
	public void setVis(Float vis) {
		this.vis = vis;
	}
	
	public String getWeatherDesc() {
		return weatherDesc;
	}
	public void setWeatherDesc(String weatherDesc) {
		this.weatherDesc = weatherDesc;
	}
	public Date getDateTime() {
		return dateTime;
	}
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getWindCdirFull() {
		return windCdirFull;
	}
	public void setWindCdirFull(String windCdirFull) {
		this.windCdirFull = windCdirFull;
	}
	public Integer getTs() {
		return ts;
	}
	public void setTs(Integer ts) {
		this.ts = ts;
	}
	public Integer getCityId() {
		return cityId;
	}
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}
	public Integer getWeatherCode() {
		return weatherCode;
	}
	public void setWeatherCode(Integer weatherCode) {
		this.weatherCode = weatherCode;
	}
	
	
	
	

}
