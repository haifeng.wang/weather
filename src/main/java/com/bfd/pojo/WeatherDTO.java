package com.bfd.pojo;

import java.lang.reflect.Array;

public class WeatherDTO {
	
/*	city_name（字符串，可选）：城市名 ，
	state_code（字符串，可选）：州缩写 ，
	country_code（字符串，可选）：国家缩写 ，
	lat（字符串，可选）：纬度 ，
	lon（字符串，可选）：经度 ，
	时区（字符串，可选）：本地IANA时区 ，
	数据（数组[ForecastHour]，可选）*/
	
	private String city_name;
	private String city_id;
	private String state_code;
	private String country_code;
	private String lat; 
	private String lon;
	private String timezone;
	private Array data;
	public String getCity_name() {
		return city_name;
	}
	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}
	public String getCity_id() {
		return city_id;
	}
	public void setCity_id(String city_id) {
		this.city_id = city_id;
	}
	public String getState_code() {
		return state_code;
	}
	public void setState_code(String state_code) {
		this.state_code = state_code;
	}
	public String getCountry_code() {
		return country_code;
	}
	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	public Array getData() {
		return data;
	}
	public void setData(Array data) {
		this.data = data;
	}
	
	
	

}
