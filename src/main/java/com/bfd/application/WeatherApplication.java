package com.bfd.application;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.retry.annotation.EnableRetry;

@EnableRetry
@SpringBootApplication
@ComponentScan("com.bfd")
@MapperScan("com.bfd.dao")
public class WeatherApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(WeatherApplication.class, args);
	}

}
