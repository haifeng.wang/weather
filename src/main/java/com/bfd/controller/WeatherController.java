package com.bfd.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bfd.pojo.WeatherDTO;
import com.bfd.service.WeatherService;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

@RestController
@RequestMapping("/weather")
public class WeatherController {
	private static Logger logger = LoggerFactory.getLogger(WeatherController.class);
	
	@Autowired
	private WeatherService weatherService;
	
	@RequestMapping("/city/{cityList}")
	public String getWeatherInfo(@PathVariable List<String> cityList) {
		String retInfo=null;
		if(cityList.size()==0 || cityList.isEmpty()){
			logger.error("请求参数不能为空");
			return retInfo;
		}
		//每次查询前先删除7天前的数据
		weatherService.deleteHistoricalInfo();
		HashMap<String,String> param = new HashMap<String,String>();
		CacheHandler.loadCache();
		String key = CacheHandler.getKey();
		String serverPath = CacheHandler.getWeatherURL();
		String lang = CacheHandler.getLang();
		param.put("lang", lang);
		param.put("key",key);
		for(String cityId:cityList){
			param.put("city_id", cityId);
			long stime = System.currentTimeMillis();
			Response response = get(serverPath, param);
			while(!response.isSuccessful() ){//默认由于key失效（返回码403），尝试下一个key,直到所有的key都尝试完
				param.remove("key");
				CacheHandler.removeKey(key);//从缓存中删除失效的ke
				key = CacheHandler.getKey();
				if(null == key || key.isEmpty()){
					logger.error("there is no available APIKey");
					return "request rejected ,there is no available APIKey";
				}
				param.put("key", key);
				response = get(serverPath, param);
			}
			String msg = response.message();
			String url = response.request().url().url().toString();
			long etime = System.currentTimeMillis();
			String info = null;
			try {
				ResponseBody body = response.body();
				info = body.string();
				logger.info("request  ->" + url + "->response:"+msg+"->spendTime : " + (etime - stime) + "ms");
			} catch (IOException e) {
				logger.error("request fail url->",url);
				logger.error("request fail", e);
			}
			
			weatherService.saveWeatherInfo(info,Integer.valueOf(cityId));
			param.remove("city_id");
			CacheHandler.clean();
			retInfo += "{request_url-->"+url+"||response_msg-->"+msg+"}";
		}
		return retInfo;
	}
	
	@RequestMapping("/hello")
	  public String test() {
	    return "hello,nihao!";
	  }
	  
	  @RequestMapping("/city/count")
	  public int count(){
		  return weatherService.count();
	  }
/**
 * 拼接请求参数，发起请求，返回响应结果
 * @param url
 * @param param
 * @return
 */
	@Retryable(value = Exception.class,maxAttempts = 5, backoff=@Backoff(delay = 10000,multiplier = 1.5))
	private static Response get(String url, Map<String, String> param) {
		url = setUrl(url, param);
		Request request = new Request.Builder().header("Content-Type", "application/json; charset=utf-8")
				.header("Accept", "application/json").url(url).get().build();

		OkHttpClient client = new OkHttpClient().newBuilder().retryOnConnectionFailure(true).connectTimeout(200, TimeUnit.SECONDS).readTimeout(600, TimeUnit.SECONDS).build();
		Response response = null;
		try {
			response = client.newCall(request).execute();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("request failed url->",url);
			logger.error("request failed", e);
		}
		/*try (Response response = client.newCall(request).execute()) {
			long etime = System.currentTimeMillis();
			String msg = null;
			if (response.isSuccessful()) {
				msg = response.body().string();
				logger.info("request successful context->" + msg + " Time : " + (etime - stime) + "ms");
				return msg;
			} else {
				logger.info("request error context->" + response.isSuccessful() + " Time : " + (etime - stime) + "ms");
			}
		}
		catch (IOException e) {
			logger.error("request failed", e);
		}*/
		return response;
	}

	private static String setUrl(String url, Map<String, String> param) {
		StringBuffer sb = new StringBuffer();
		sb.append("?");
		if (param != null) {
			Set<String> keySet = param.keySet();
			for (String key : keySet) {
				sb = sb.append(key).append("=").append(param.get(key)).append("&");
			}
		}
		sb.substring(0, sb.lastIndexOf("&"));
		return url + sb.toString();
	}
}
