package com.bfd.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="weather")
public class CacheHandler {
	
	//@Value("${weather.keys}")
	public static String keys;
	//@Value("${weather.lang}")
	private static String lang;
	//@Value("${weather.weatherURL}")
	private static String weatherURL;
	
	private static List<String> cache = new ArrayList<String>();
	
	public static void loadCache() {
		String[] lists = keys.split(",");
		for(String key:lists) {
			cache.add(key);
		}
	}
	
	public static String getKey() {
		Random rand = new Random();
		if(0 != cache.size()) {
			int index = rand.nextInt(cache.size());
			return cache.get(index);
		}
		return null;
	}
	public static void removeKey(String key) {
		cache.remove(key);
	}
	//重新装载之前的environment
	public static void clean() {
		cache.clear();
	}
	
	public static String getLang() {
		return lang;
	}
	public static void setLang(String lang) {
		CacheHandler.lang = lang;
	}
	public static String getKeys() {
		return keys;
	}
	public static void setKeys(String keys) {
		CacheHandler.keys = keys;
	}
	public static String getWeatherURL() {
		return weatherURL;
	}
	public static void setWeatherURL(String weatherURL) {
		CacheHandler.weatherURL = weatherURL;
	}



}
